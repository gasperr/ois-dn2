var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal, isPass, pass) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    isPass: isPass,
    pass: pass
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var all = besede.join(" ");
      
      if(countOccurances(all) < 3 || all.match(/ "([^"]+)"/) == null){
        this.spremeniKanal(all, false, " ");
        break;
      }
      else{
        var kanal = all.match(/"([^"]+)"/);
        if(kanal != null) {
          kanal = kanal[1];
        } else kanal = "";
        
        var pass = all.match(/ "([^"]+)"/);
        if(pass != null){
          pass = pass[1];
        } else pass = "";
        

        this.spremeniKanal(kanal, true, pass);
        break;
      }
      
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
      
    case 'zasebno':
      besede.shift();
      
      ukaz = besede.join(" ");

      var prejemnik = ukaz.match(/[^\"]+/);
      ukaz = ukaz.replace(/[^\"]+/, "");
      ukaz = ukaz.replace(/["]/g, "");
      
      this.socket.emit('zasebnoSporocilo', prejemnik, ukaz);
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};

function countOccurances(string){
  var st = 0;
  for(var i = 0; i < string.length-1; i++){
    if(string.charAt(i) == "\"" && string.charAt(i+1) != "\"") st++;
  }
  return st;
}
