function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementEnostavniTekstSmiley(sporocilo) {
  var x = $('<div style="font-weight: bold"></div>').append(sporocilo);
  $('#sporocila').append(x);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function divElementZasebnoSp(sporocilo){
  var x = $('<div style="color:#0000CC"></div>').append(sporocilo);
  $('#sporocila').append(x);
}

function divElementZasebnoSpPrejeto(sporocilo){
  var x = $('<div style="color:#0099CC"></div>').append(sporocilo);
  $('#sporocila').append(x);
}

function divElementEnostavniTekstPass(sporocilo) {
  return $('<div style="color: #d00000; font-weight: bold"></div>').text(sporocilo);
}

var trenutniKanal;

function obdelajSmajlije(sporocilo){
    var smiley = sporocilo.indexOf(":)");
    var wink = sporocilo.indexOf(";)");
    var like = sporocilo.indexOf("(y)");
    var kiss = sporocilo.indexOf(":*");
    var sad = sporocilo.indexOf(":(");
    

      var indexOkl = sporocilo.indexOf("<");
      if(indexOkl > -1){
        sporocilo = sporocilo.split("<").join("&lt");
      }
      if(smiley > -1){
        sporocilo = sporocilo.split(":)").join("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png'>");
      }
      if(wink > -1){
        sporocilo = sporocilo.split(";)").join("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png'>");
      }
      if(like > -1){
        sporocilo = sporocilo.split("(y)").join("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png'>");
      }
      if(kiss > -1){
        sporocilo = sporocilo.split(":*").join("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png'>");
      }
      if(sad > -1){
        sporocilo = sporocilo.split(":(").join("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png'>");
      }
      
      return sporocilo;
}
  


function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {


    
    var kanal = trenutniKanal;
    
    klepetApp.posljiSporocilo(kanal, sporocilo);
    

    var smiley = sporocilo.indexOf(":)");
    var wink = sporocilo.indexOf(";)");
    var like = sporocilo.indexOf("(y)");
    var kiss = sporocilo.indexOf(":*");
    var sad = sporocilo.indexOf(":(");
    sporocilo = censureCurse(sporocilo);
    if(smiley > -1 || wink > -1 || like > -1 || kiss > -1 || sad > -1){
      sporocilo = obdelajSmajlije(sporocilo)
      
      divElementEnostavniTekstSmiley(sporocilo);

    }
    
    else{
      var bes = censureCurse(sporocilo);
      $('#sporocila').append(divElementEnostavniTekst(bes));
    } 

    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    
    
  }

  $('#poslji-sporocilo').val('');
}

function replaceAt(index, string){
  var prviDel = string.substring(0, index);
  var drugiDel = string.substring(index+1, string.length);
  return prviDel + "*" + drugiDel;
}

function censureCurse(string){
  $(document).ready(function(){
    $.ajax({
     type: "GET",
     url: "swearWords.xml",
     dataType: "xml",
     async: false,
     success: function(xml){
       
       $(xml).find('words').each(function(){
            $(this).find('word').each(function(){
              //string = " "+string+" ";
              var sw = $(this).text();
              //sw = " "+sw+" ";
              var re = new RegExp('\\b'+sw+'\\b');
              
             
              while(string.search(re) != -1){
                var x = string.search(re);
                for(var i = x; i < x+sw.length; i++){
                  string = replaceAt(i, string);
                }
               
              }
            
          });
       });
     },
     error: function(){
       alert("Can't reach file.");
     }
     
     
    });
    
    
  });
  return string;
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  socket.on('obdelajZasebnoSporociloOdgovor', function(rezultat){
    var sporocilo;
    
    rezultat.sporocilo = censureCurse(rezultat.sporocilo);
    rezultat.sporocilo = obdelajSmajlije(rezultat.sporocilo);
    
    if(rezultat.uspesno){
      sporocilo = '(Zasebno za '+rezultat.prejemnik+' ['+rezultat.prejemniskiKanal+']): '+rezultat.sporocilo;
      
    } else{
      sporocilo = rezultat.sporocilo;
      
    }
    $('#sporocila').append(divElementZasebnoSp(sporocilo));
    
  });
  
  socket.on('zasebnoSporociloSporocilo', function(rezultat){
    
    rezultat.sporocilo = censureCurse(rezultat.sporocilo);
    rezultat.sporocilo = obdelajSmajlije(rezultat.sporocilo);
    
    var sporocilo = rezultat.posiljatelj+' ['+rezultat.kanal+'] (zasebno): '+rezultat.sporocilo;
    $('#sporocila').append(divElementZasebnoSpPrejeto(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  

  socket.on('vzdevekKanal', function(rezultat){
     $('#kanal').text(rezultat.vzdevek +" @ "+rezultat.kanal);
  });

  


  socket.on('sporocilo', function (sporocilo) {

    
    var smiley = sporocilo.besedilo.indexOf(":)");
    var wink = sporocilo.besedilo.indexOf(";)");
    var like = sporocilo.besedilo.indexOf("(y)");
    var kiss = sporocilo.besedilo.indexOf(":*");
    var sad = sporocilo.besedilo.indexOf(":(");
    sporocilo.besedilo = censureCurse(sporocilo.besedilo);
    if(smiley > -1 || wink > -1 || like > -1 || kiss > -1 || sad > -1){
      sporocilo.besedilo = obdelajSmajlije(sporocilo.besedilo);
      
      
      var nov = $('<div style="font-weight: bold"></div>').append(sporocilo.besedilo);
      $('#sporocila').append(nov);
    }else{
      var bes = censureCurse(sporocilo.besedilo);
      var novElement = $('<div style="font-weight: bold"></div>').text(bes);
      $('#sporocila').append(novElement);
    }
  });
  
  socket.on('uporabniki', function(uporabniki){
    $('#seznam-uporabnikov').empty();
  
    for(var i = 0; i < uporabniki.length; i++){
      $('#seznam-uporabnikov').append(divElementHtmlTekst(uporabniki[i]));
    }
    
    $('#seznam-uporabnikov div').click(function() {
      var up = $(this).text();
      var msg = prompt("Vnesite zasebno sporocilo za "+up+":");
      if(msg != null && msg != "") klepetApp.procesirajUkaz('/zasebno \"' + up+'\" \"' + msg+'\"');
      
      
    });
      

    
  });

     
  

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i = 0; i < kanali.length; i++) {
      if(kanali[i] == undefined) continue;
      if(kanali[i].isPass) $('#seznam-kanalov').append(divElementEnostavniTekstPass(kanali[i].ime));
      else $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i].ime));
    }

    $('#seznam-kanalov div').click(function() {
      var kanal = $(this).text();
      var isPass = false;
      for(var i = 0; i < kanali.length; i++){
        if(kanali[i] == undefined) continue;
        if(kanali[i].ime == kanal) isPass = kanali[i].isPass;
      }
      var pass = " \" \"";
      if(isPass){
        pass = prompt("Za pridružitev je potrebno geslo:");
        klepetApp.procesirajUkaz('/pridruzitev \"' + $(this).text()+'\" \"' + pass+'\"');
      } else{
        klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      }
      
      $('#poslji-sporocilo').focus();
    });
  });
  

  socket.on('trenutniKanal', function(rez){
    trenutniKanal = rez;
    
  });



  setInterval(function() {
    socket.emit('kanali');
    socket.emit('trenutniKanal');
  }, 1000);
  
  setInterval(function() {
    var kanal = $('#kanal').text();
    kanal = kanal.match(/@\s*.*/)
    kanal = kanal[0].substring(2, kanal[0].length);
    socket.emit('uporabniki', kanal);
  }, 1000);


  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});