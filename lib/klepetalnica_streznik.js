var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};


var vsiKanali = [];
var kanalID = 0;


exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj', false, " ");
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajZasebnoSporocilo(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', vsiKanali);
    });
    
    socket.on('kanaliVsi', function(){
      socket.emit('kanaliVsi', vsiKanali)
    });

    socket.on('uporabniki', function(rez) {
      socket.emit('uporabniki', posredujUporabnike(socket, rez));  

    });

    socket.on('trenutniKanal', function(){
      socket.emit('trenutniKanal', trenutniKanal[socket.id]);
    });

    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};


function kanalObstaja(kanal){
  for(var i = 0; i < vsiKanali.length; i++){
    if(vsiKanali[i] === undefined) continue;
    if(vsiKanali[i].ime == kanal) return true;
  }
  return false;
}


function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  socket.emit('vzdevekKanal', {vzdevek: vzdevek, kanal: trenutniKanal[socket.id]});
  uporabljeniVzdevki.push(vzdevek);
  
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal, isPass, pass) {
  
  
  if(!kanalObstaja(kanal)){
    vsiKanali.push({
      ime: kanal,
      isPass: isPass,
      pass: pass,
      id: kanalID
    });
    kanalID++;
  }
  
  socket.join(kanal);
  
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  socket.emit('vzdevekKanal', {vzdevek: vzdevkiGledeNaSocket[socket.id], kanal: kanal});


  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}


function posredujUporabnike(socket, kanal){

  var vzdevki = [];
  
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      
      vzdevki.push(vzdevkiGledeNaSocket[uporabnikSocketId]);
  }
  
  return vzdevki;
}

function pridruzitevNaKanal(socket, kanal, isPass, pass) {
  
  
  if(kanalObstaja(kanal)){
    if(findArg(kanal, "isPass") == false && isPass == true){
      socket.emit('sporocilo', {besedilo: "Izbrani kanal "+kanal+" je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev "+kanal+" ali zahtevajte kreiranje kanala z drugim imenom."});
 
    }
    else if(findArg(kanal, "pass") != pass){
      socket.emit('sporocilo', {besedilo: "Pridružitev v kanal "+kanal+" ni bilo uspešno, ker je geslo napačno!"});

    }
    else if(findArg(kanal, "pass") == pass){
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal, isPass, pass);
    }
  }
  else{
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanalu(socket, kanal, isPass, pass);
  }
  

}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo zaÄeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.emit('vzdevekKanal', {vzdevek: vzdevek, kanal: trenutniKanal[socket.id]});
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je Å¾e v uporabi.'
        });
      }
    }
  });
}



function returnSocketID(socket, name){
  var upNaKanalu = io.sockets.clients();
  
  for(var i in upNaKanalu){
    if(vzdevkiGledeNaSocket[upNaKanalu[i].id] == name) return upNaKanalu[i];
  }
  
  return false;
}

function upObstaja(nickName, array){
  
  for(var i = 0; i < array.length; i++){
    if(nickName == array[i]) return true;
  }
  
  return false;
  
}



function obdelajZasebnoSporocilo(socket){
  
  
  socket.on('zasebnoSporocilo', function(prejemnik, msg){
    var vsiUporabniki = posredujUporabnike();

    if(!upObstaja(prejemnik, vsiUporabniki) || vzdevkiGledeNaSocket[socket.id] == prejemnik){
      socket.emit('obdelajZasebnoSporociloOdgovor', {
        uspesno: false,
        sporocilo: 'Sporočila '+msg+' uporabniku z vzdevkom '+prejemnik+' ni bilo mogoče posredovati.'
       
      });
    } else {
      returnSocketID(socket, prejemnik).emit('zasebnoSporociloSporocilo', {
        posiljatelj: vzdevkiGledeNaSocket[socket.id],
        sporocilo: msg,
        kanal: trenutniKanal[socket.id]
      });
      socket.emit('obdelajZasebnoSporociloOdgovor', {
        uspesno: true,
        sporocilo: msg,
        prejemnik: prejemnik,
        prejemniskiKanal: trenutniKanal[returnSocketID(socket, prejemnik).id]
      })
      
    }
    
    
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function findArg(kanal, arg){
  for(var i = 0; i < vsiKanali.length; i++){
    if(vsiKanali[i] === undefined) continue;
    if(vsiKanali[i].ime == kanal){
      if(arg == 'id') return vsiKanali[i].id;
      else if(arg == 'isPass') return vsiKanali[i].isPass;
      else if(arg == 'pass') return vsiKanali[i].pass;
      
    }
  }
  return null;
}

setInterval(function(){
  for(var i = 0; i < vsiKanali.length; i++){
    if(vsiKanali[i] == undefined) continue;
    var obstaja = false;
    for(var kanal in io.sockets.manager.rooms) {
      kanal = kanal.substring(1, kanal.length);
      if(vsiKanali[i].ime == kanal){
        obstaja = true;
        break;
      }
    }
    if(!obstaja){
      delete vsiKanali[vsiKanali[i].id];
    }
  }
}, 500);

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    pridruzitevNaKanal(socket, kanal.novKanal, kanal.isPass, kanal.pass);
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
    
  });
}